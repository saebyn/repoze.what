# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007, Agendaless Consulting and Contributors.
# Copyright (c) 2008, Florent Aide <florent.aide@gmail.com>.
# Copyright (c) 2008-2009, Gustavo Narea <me@gustavonarea.net>.
# All Rights Reserved.
#
# This software is subject to the provisions of the BSD-like license at
# http://www.repoze.org/LICENSE.txt.  A copy of the license should accompany
# this distribution.  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL
# EXPRESS OR IMPLIED WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND
# FITNESS FOR A PARTICULAR PURPOSE.
#
##############################################################################

"""Tests for the base source adapters."""

import unittest

from zope.interface import implements

from repoze.what.adapters import *

from .base import FakeGroupSourceAdapter


class TestBaseSourceAdapter(unittest.TestCase):
    """
    Tests for the base source adapter.
    
    The most important thing to be checked is that it deals with its internal
    cache correctly.
    
    """
    
    def setUp(self):
        self.adapter = FakeGroupSourceAdapter()
        
    def test_cache_is_empty_initially(self):
        """No section has been loaded; the cache is clear"""
        self.assertEqual(self.adapter.loaded_sections, {})
        self.assertEqual(self.adapter.all_sections_loaded, False)
        
    def test_items_are_returned_as_sets(self):
        """The items of a section must always be returned as a Python set"""
        # Bulk fetch:
        for section in self.adapter.get_all_sections():
            assert isinstance(self.adapter.get_section_items(section), set)
    
    def test_retrieving_all_sections(self):
        self.assertEqual(self.adapter.get_all_sections(),
                         self.adapter.fake_sections)
        # Sections are in the cache now
        self.assertEqual(self.adapter.loaded_sections,
                         self.adapter.fake_sections)
        self.assertEqual(self.adapter.all_sections_loaded, True)
    
    def test_getting_section_items(self):
        self.assertEqual(self.adapter.get_section_items('trolls'), 
                         self.adapter.fake_sections['trolls'])
    
    def test_getting_items_of_non_existing_section(self):
        self.assertRaises(NonExistingSectionError, 
                          self.adapter.get_section_items,
                          'non-existing')
    
    def test_setting_section_items(self):
        items = ('guido', 'rasmus')
        self.adapter.set_section_items('trolls', items)
        self.assertEqual(self.adapter.fake_sections['trolls'], set(items))
    
    def test_cache_is_updated_after_setting_section_items(self):
        # Loading for the first time:
        self.adapter.get_section_items('developers')
        # Adding items...
        items = ('linus', 'rms')
        self.adapter.set_section_items('developers', items)
        # Checking the cache:
        self.assertEqual(self.adapter.get_section_items('developers'),
                         set(items))
    
    def test_getting_sections_by_criteria(self):
        credentials = {'repoze.what.userid': 'sballmer'}
        sections = set(['trolls'])
        self.assertEqual(self.adapter.find_sections(credentials), sections)
    
    def test_adding_one_item_to_section(self):
        self.adapter.include_item('developers', 'rasmus')
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         set(('linus', 'rasmus', 'rms')))
    
    def test_adding_many_items_to_section(self):
        self.adapter.include_items('developers', ('sballmer', 'guido'))
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         set(('rms', 'sballmer', 'linus', 'guido')))
    
    def test_cache_is_updated_after_adding_item(self):
        # Loading for the first time:
        self.adapter.get_section_items('developers')
        # Now let's add the item:
        self.adapter.include_item('developers', 'guido')
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         set(('linus', 'guido', 'rms')))
        # Now checking that the cache was updated:
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         self.adapter.get_section_items('developers'))
    
    def test_removing_one_item_from_section(self):
        self.adapter.exclude_item('developers', 'linus')
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         set(['rms']))
    
    def test_removing_many_items_from_section(self):
        self.adapter.exclude_items('developers', ('linus', 'rms'))
        self.assertEqual(self.adapter.fake_sections['developers'], set())
    
    def test_cache_is_updated_after_removing_item(self):
        # Loading for the first time:
        self.adapter.get_section_items('developers')
        # Now let's remove the item:
        self.adapter.exclude_item('developers', 'rms')
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         set(['linus']))
        # Now checking that the cache was updated:
        self.assertEqual(self.adapter.fake_sections['developers'], 
                         self.adapter.get_section_items('developers'))

    def test_creating_section(self):
        self.adapter.create_section('sysadmins')
        self.assertTrue('sysadmins' in self.adapter.fake_sections)
        self.assertEqual(self.adapter.fake_sections['sysadmins'],
                         set())
    
    def test_creating_existing_section(self):
        self.assertRaises(ExistingSectionError, self.adapter.create_section,
                          'developers')
    
    def test_cache_is_updated_after_creating_section(self):
        self.adapter.create_section('sysadmins')
        self.assertEqual(self.adapter.get_section_items('sysadmins'), set())
    
    def test_editing_section(self):
        items = self.adapter.fake_sections['developers']
        self.adapter.edit_section('developers', 'designers')
        self.assertEqual(self.adapter.fake_sections['designers'], items)
    
    def test_editing_non_existing_section(self):
        self.assertRaises(NonExistingSectionError, self.adapter.edit_section,
                          'this_section_doesnt_exit', 'new_name')
    
    def test_cache_is_updated_after_editing_section(self):
        # Loading for the first time:
        self.adapter.get_section_items('developers')
        # Editing:
        description = 'Those who write in weird languages'
        items = self.adapter.fake_sections['developers']
        self.adapter.edit_section('developers', 'coders')
        # Checking cache:
        self.assertEqual(self.adapter.get_section_items('coders'), items)
    
    def test_deleting_section(self):
        self.adapter.delete_section('developers')
        self.assertRaises(NonExistingSectionError,
                          self.adapter.get_section_items, 'designers')
    
    def test_deleting_non_existing_section(self):
        self.assertRaises(NonExistingSectionError, self.adapter.delete_section,
                          'this_section_doesnt_exit')
    
    def test_cache_is_updated_after_deleting_section(self):
        # Loading for the first time:
        self.adapter.get_section_items('developers')
        # Deleting:
        self.adapter.delete_section('developers')
        # Checking cache:
        self.assertRaises(NonExistingSectionError,
                          self.adapter.get_section_items,
                          'developers')
    
    def test_checking_section_existence(self):
        # Existing section:
        self.adapter._check_section_existence('developers')
        # Non-existing section:
        self.assertRaises(NonExistingSectionError,
                          self.adapter._check_section_existence, 'designers')
    
    def test_checking_section_not_existence(self):
        # Non-existing section:
        self.adapter._check_section_not_existence('designers')
        # Existing section:
        self.assertRaises(ExistingSectionError,
                          self.adapter._check_section_not_existence, 'admins')
    
    def test_checking_item_inclusion(self):
        self.adapter._confirm_item_is_present('developers', 'linus')
        self.assertRaises(ItemNotPresentError,
                          self.adapter._confirm_item_is_present, 'developers', 
                          'maribel')
    
    def test_checking_item_inclusion_in_non_existing_section(self):
        self.assertRaises(NonExistingSectionError,
                          self.adapter._confirm_item_is_present, 'users', 
                          'linus')
    
    def test_checking_item_exclusion(self):
        self.adapter._confirm_item_not_present('developers', 'maribel')
        self.assertRaises(ItemPresentError,
                          self.adapter._confirm_item_not_present, 
                          'developers', 'linus')
    
    def test_checking_item_exclusion_in_non_existing_section(self):
        self.assertRaises(NonExistingSectionError,
                          self.adapter._confirm_item_is_present, 'users', 
                          'linus')


class TestBaseSourceAdapterAbstract(unittest.TestCase):
    """
    Tests for the base source adapter's abstract methods.
    
    """
    
    def setUp(self):
        self.adapter = BaseSourceAdapter()
        
    def test_get_all_sections(self):
        self.assertRaises(NotImplementedError, self.adapter._get_all_sections)
        
    def test_get_section_items(self):
        self.assertRaises(NotImplementedError, self.adapter._get_section_items,
                          None)
        
    def test_find_sections(self):
        self.assertRaises(NotImplementedError, self.adapter._find_sections,
                          None)
        
    def test_include_items(self):
        self.assertRaises(NotImplementedError, self.adapter._include_items,
                          None, None)
        
    def test_exclude_items(self):
        self.assertRaises(NotImplementedError, self.adapter._exclude_items,
                          None, None)
        
    def test_item_is_included(self):
        self.assertRaises(NotImplementedError, self.adapter._item_is_included,
                          None, None)
        
    def test_create_section(self):
        self.assertRaises(NotImplementedError, self.adapter._create_section,
                          None)
        
    def test_edit_section(self):
        self.assertRaises(NotImplementedError, self.adapter._edit_section,
                          None, None)
        
    def test_delete_section(self):
        self.assertRaises(NotImplementedError, self.adapter._delete_section,
                          None)
        
    def test_section_exists(self):
        self.assertRaises(NotImplementedError, self.adapter._section_exists,
                          None)
    
    def test_adapter_is_writable_by_default(self):
        self.assertTrue(self.adapter.is_writable)


class TestNotWritableSourceAdapter(unittest.TestCase):
    """Tests for an adapter dealing with a read-only source"""
    
    def setUp(self):
        self.adapter = FakeGroupSourceAdapter(writable=False)
        
    def test_setting_items(self):
        self.assertRaises(SourceError, 
                          self.adapter.set_section_items,
                          'admins', ['gnu', 'tux'])
        
    def test_settings_items_in_non_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(NonExistingSectionError, 
                          self.adapter.set_section_items,
                          'mascots', ['gnu', 'tux'])
        
    def test_include_items(self):
        self.assertRaises(SourceError, self.adapter.include_item,
                          'admins', 'tux')
        self.assertRaises(SourceError, self.adapter.include_items,
                          'admins', ['gnu', 'tux'])
        
    def test_include_items_in_non_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(NonExistingSectionError, self.adapter.include_item,
                          'mascots', 'gnu')
        self.assertRaises(NonExistingSectionError, self.adapter.include_items,
                          'mascots', ['gnu', 'tux'])
        
    def test_include_existing_items(self):
        """The items existence must be checked first"""
        self.assertRaises(ItemPresentError, self.adapter.include_item,
                          'developers', 'rms')
        self.assertRaises(ItemPresentError, self.adapter.include_items,
                          'developers', ['rms', 'linus'])
        
    def test_exclude_items(self):
        self.assertRaises(SourceError, self.adapter.exclude_item,
                          'admins', 'rms')
        self.assertRaises(SourceError, self.adapter.exclude_items,
                          'developers', ['rms', 'linus'])
        
    def test_exclude_items_in_non_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(NonExistingSectionError, self.adapter.exclude_item,
                          'mascots', 'gnu')
        self.assertRaises(NonExistingSectionError, self.adapter.exclude_items,
                          'mascots', ['gnu', 'tux'])
        
    def test_exclude_existing_items(self):
        """The items existence must be checked first"""
        self.assertRaises(ItemNotPresentError, self.adapter.exclude_item,
                          'developers', 'rasmus')
        self.assertRaises(ItemNotPresentError, self.adapter.exclude_items,
                          'developers', ['guido', 'rasmus'])
        
    def test_create_section(self):
        self.assertRaises(SourceError, self.adapter.create_section,
                          'mascots')
        
    def test_create_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(ExistingSectionError, self.adapter.create_section,
                          'admins')
        
    def test_edit_section(self):
        self.assertRaises(SourceError, self.adapter.edit_section,
                          'admins', 'administrators')
        
    def test_edit_non_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(NonExistingSectionError, self.adapter.edit_section,
                          'mascots', 'animals')
        
    def test_delete_section(self):
        self.assertRaises(SourceError, self.adapter.delete_section,
                          'admins')
        
    def test_delete_non_existing_section(self):
        """The section existence must be checked first"""
        self.assertRaises(NonExistingSectionError, self.adapter.delete_section,
                          'mascots')
    
    def test_adapter_is_not_writable(self):
        self.assertFalse(self.adapter.is_writable)
